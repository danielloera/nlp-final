import os
import json
import argparse
from utils import Indexer
from language_ex import *
from typing import List
from classifiers import *

def _parse_args():
    parser = argparse.ArgumentParser(description='trainer.py')
    parser.add_argument('--model', type=str, default='Freq', help='model to run (Freq, NB, RNN)')
    parser.add_argument('--train', type=str, default='./data/tweet_data.json', help='file to extract training and test data.')
    parser.add_argument('--save', type=str, help='filename to save torch model to. Only applies to RNN.')
    parser.add_argument('--load', type=str, help='filename to load torch model from. Only applies to RNN.')
    parser.add_argument('--epochs', type=int, default=15, help='Number of training epochs. Only applies to RNN.')
    parser.add_argument('--test', type=str, help='data to test against. Defaults to --data.')
    parser.add_argument('--batch-size', type=int, default=100, help='Training batch size. Only applies to RNN.')
    parser.add_argument('--input', action='store_true',
                        help='if set, custom input can be tested after training from the command line.')

    args = parser.parse_args()
    return args

def create_indexer(objs):
    i = Indexer()
    for o in objs:
        i.add_and_get_index(o)
    return i

def read_data(file_name: str) -> List[LabeledSentence]:
    vocab_indexer = Indexer()
    vocab_indexer.add_and_get_index('<PAD>')
    observations = []
    with open(file_name, 'r') as fi:
        twt_dict = json.load(fi)
        labeled_tokens = []

        for item in twt_dict:
            observations.append(LabeledSentence(
                item[1]["text"], item[1]["labels"]))

            for word in item[1]["text"]:
                vocab_indexer.add_and_get_index(word)

    return vocab_indexer, observations

def eval(model: LanguageClassifier, data: List[LabeledSentence]):
    predictions = [model.predict(ls.words) for ls in data]
    golds = [ls.labels for ls in data]

    if len(golds) != len(predictions):
        raise Exception("Mismatched gold/pred lengths: %i / %i" % (len(golds), len(predictions)))

    num_total = 0
    num_correct = 0
    num_en = 0
    num_es = 0
    tp_dict = {'en': 0, 'es': 0, 'other': 0}
    fp_dict = {'en': 0, 'es': 0, 'other': 0}
    fn_dict = {'en': 0, 'es': 0, 'other': 0}

    for idx in range(0, len(golds)):
        gold = golds[idx]
        prediction = predictions[idx]
        for i in range(len(gold)):
            p = prediction[i]
            g = gold[i]

            is_correct = p == g
            num_correct += int(is_correct)
            num_total += 1

            if is_correct:
                # correctly predicted g
                tp_dict[g] += 1
            else:
                # incorrectly predicted g as p
                fp_dict[p] += 1
                fn_dict[g] += 1

            if g == 'en':
                num_en += 1

            if g == 'es':
                num_es += 1

    assert num_en == tp_dict['en'] + fn_dict['en']
    assert num_es == tp_dict['es'] + fn_dict['es']

    eval_print(num_correct, num_total, tp_dict['en'], tp_dict['es'],
                fn_dict['en'], fn_dict['es'], fp_dict['en'], fp_dict['es'],
                tp_dict['en'] + fn_dict['en'], tp_dict['es'] + fn_dict['es'])

def eval_print(num_correct, num_total, num_en_correct, num_es_correct,
                wrong_not_en, wrong_not_es, wrong_en, wrong_es,
                num_en_gold, num_es_gold):
    print("OVERALL:")
    print("Accuracy: %i / %i = %f\n" % (num_correct, num_total, float(num_correct) / num_total))
    print("ENGLISH:")
    print("Accuracy: {} / {} = {}".format(num_en_correct, num_en_gold, float(num_en_correct) / num_en_gold))
    prec_en = float(num_en_correct) / (num_en_correct + wrong_en)
    rec_en = float(num_en_correct) / (num_en_correct + wrong_not_en)
    f1_en = 2 * prec_en * rec_en / (prec_en + rec_en) if prec_en > 0 and rec_en > 0 else 0.0
    print("Precision: %i / %i = %f" % (num_en_correct, num_en_correct + wrong_en, prec_en))
    print("Recall: %i / %i = %f" % (num_en_correct, num_en_correct + wrong_not_en, rec_en))
    print("F1: %f\n" % f1_en)
    print("SPANISH:")
    print("Accuracy: {} / {} = {}".format(num_es_correct, num_es_gold, float(num_es_correct) / num_es_gold))
    prec_es = float(num_es_correct) / (num_es_correct + wrong_es)
    rec_es = float(num_es_correct) / (num_es_correct + wrong_not_es)
    f1_es = 2 * prec_es * rec_es / (prec_es + rec_es) if prec_es > 0 and rec_es > 0 else 0.0
    print("Precision: %i / %i = %f" % (num_es_correct, num_es_correct + wrong_es, prec_es))
    print("Recall: %i / %i = %f" % (num_es_correct, num_es_correct + wrong_not_es, rec_es))
    print("F1: %f" % f1_es)

if __name__ == '__main__':
    args = _parse_args()
    print(args)

    vocab_indexer, observations = read_data(args.train)
    test_obs = None
    if args.test:
        _, test_obs = read_data(args.test)
    # <PAD> has to have index 0
    label_indexer = create_indexer(['<PAD>','en', 'es', 'other'])
    ten_per = int(len(observations) * 0.20)
    test_data = observations[:ten_per]
    train_data = observations[ten_per:]
    print(len(observations), "observations\n")

    model = None
    if args.model == "Freq":
        model = FrequencyClassifier()
    elif args.model == "NB":
        model = NaiveBayesClassifier(label_indexer)
    elif args.model == "RNN":
        model = RNNClassifier(vocab_indexer, label_indexer,
                              test_data=test_data,
                              num_epochs=args.epochs,
                              batch_size=args.batch_size,
                              save=args.save,
                              load=args.load)
    if not args.load:
        model.fit(train_data)
    if not args.test:
        print("TRAIN:\n")
        eval(model, train_data)
        print('-'*45)
    if test_obs:
        test_data = test_obs
    print("TEST:\n")
    eval(model, test_data)
    if args.input:
        t = input("Enter sentence to label or 'exit': ")
        print()
        while t != 'exit':
            sent = t.split()
            pred = model.predict(sent)
            longest_word = max(len(s) for s in sent)
            for i in range(len(sent)):
                word = sent[i]
                padding = (" " * (longest_word - len(word))) + "   "
                print(f"{word}{padding}{pred[i]}")
            t = input("Enter sentence to label or 'exit': ")
            print()
