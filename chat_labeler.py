import json
import random
import pylangacq as pla

from collections import Counter

PUNC = {'.', '!', "'", '"', "?", ","}

def clean(word):
  cleaned = ""
  for c in word:
    if c.isalpha() or c in PUNC:
      cleaned += c
  return cleaned

def clean_end(word, ending):
  if ending in word:
    return word[:word.index(ending)]
  return word

OUTPUT_FILE = "data/labeled_chats.json"
SPANISH = "es"
ENGLISH = "en"
OTHER = "other"
CONVERT = {
  "eng": ENGLISH,
  "spa": SPANISH,
  "ita": SPANISH,
  "eng&spa": OTHER,
  "eng&spag": OTHER,
  "spa&eng": OTHER,
  "spa+eng": OTHER,
  "eng+spa": OTHER,
  "eng&spa+eng": OTHER
}
uids = Counter()
labeled_chats = []

def label_chats(files, default):
  count = 0
  chats = pla.read_chat(files)
  exceptiong_str = 'spa]' if default == ENGLISH else 'eng]'
  flip_str = SPANISH if default == ENGLISH else ENGLISH
  print("labeling chat files:")
  for f in chats.filenames():
    print(f)
  print()
  for utt in chats.utterances(clean=False):
    # remove utterence ending character
    words = utt[1].split()[:-1]
    default_tag = default
    speaker = utt[0]
    uid = f"{speaker}_{uids[speaker]}"
    uids[speaker] += 1
    # utterence is primarily spanish or english
    if exceptiong_str in words and '[-' in words:
      words = words[words.index(exceptiong_str) + 1:]
      default_tag = flip_str
    labeled_chat = {}
    labeled_chat["text"] = []
    labeled_chat["labels"] = []
    added_data = False
    for w in words:
      word = None
      label = None
      if '@s:' in w:
        word, label = w.split('@s:')
        label = clean_end(label, '>')
        label = clean_end(label, ',')
        label = clean_end(label, ']')
        label = CONVERT.get(label, None)
        if label is None:
          #print(f"Unkown label from utterence {utt}")
          continue
      elif any(c.isalpha() for c in w):
          word  = w
          label = default_tag
      if word and label:
        added_data = True
        labeled_chat["text"].append(clean(word))
        labeled_chat["labels"].append(label)
    if added_data:
      labeled_chats.append((uid, labeled_chat))
      count += 1
  print(f"labeled {count} primarily {default} sentences.")

label_chats(files='data/chats/en_primary/*.cha',
            default=ENGLISH)
label_chats(files='data/chats/es_primary/*.cha',
            default=SPANISH)

print("labeled", len(labeled_chats), "sentences")
print('writing files...')
with open(OUTPUT_FILE, 'w+') as output:
    json.dump(labeled_chats, output)
parsed = None
with open(OUTPUT_FILE) as read_output:
    parsed = json.load(read_output)
random.shuffle(parsed)
with open(OUTPUT_FILE, 'w+') as output:
    json.dump(parsed, output, indent=2)
print("DONE")