import json
import sys
import random

OUTPUT_FILE = "combined_data.json"

file_1 = sys.argv[1]
file_2 = sys.argv[2]

with open(file_1) as f1:
    list1 = json.load(f1)
with open(file_2) as f2:
    list2 = json.load(f2)

combined = list1 + list2
random.shuffle(combined)

with open(OUTPUT_FILE, 'w+') as out_file:
    json.dump(combined, out_file, indent=2)

print(f"combined {len(combined)} records.")