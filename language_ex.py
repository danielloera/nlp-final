from typing import List
from utils import Indexer

class LabeledSentence(object):
    def __init__(self, words: List[str], labels: List[str]):
        self.words = words
        self.labels = labels
    
    def get_lbled_w(self):
        return zip(self.words, self.labels)

    def word_idx_vec(self, word_indexer: Indexer) -> List[int]:
        return [word_indexer.index_of(word) for word  in self.words]

    def gold_label_vec(self, label_indexer: Indexer) -> List[int]:
        return [label_indexer.index_of(label) for label  in self.labels]
