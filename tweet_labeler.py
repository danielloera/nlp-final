import json
import random

REPLACED_LABELS = { "ne", "ambiguous" }
IGNORED_LABELS = { "other" }
SPANISH_UNICODE = {
    "Á", "á", "É", "é", "Í", "í", "Ó", "ó", "Ú",
    "ú", "Ü", "ü", "Ñ", "ñ", "ª", "º", "¡" ,"¿"
}

def clean_data(t, l):
    text = []
    labels = []
    for i in range(len(l)):
        label = l[i]
        if label in IGNORED_LABELS:
            continue
        if label in REPLACED_LABELS:
            label = "other"
        word = t[i]
        if word and word[0] == "#":
            word = word[1:]
        if label == "mixed":
            label = "other"
            for c in word:
                if c in SPANISH_UNICODE:
                    label = "es"
                    break
        text.append(word)
        if label == 'english':
            label = 'en'
        labels.append(label)
    return text, labels


file_pairs = [
    ('./data/dataframes/en_es_test_offsets.tsv',
     './data/dataframes/test_tweets.tsv',
     './data/labeled_test.json'),
    ('./data/dataframes/en_es_training_offsets.tsv',
     './data/dataframes/train_tweets_raw.tsv',
     './data/tweet_data.json')
]

for pair in file_pairs:
    print("labeling for:", pair)
    offset_file = open(pair[0])
    tweet_file = open(pair[1])
    offset_lines = offset_file.readlines()
    tweet_lines = tweet_file.readlines()
    offset_file.close()
    tweet_file.close()

    offset_dict = {}
    print('reading offsets')
    for line in offset_lines:
        tid, user, off1, off2, label = line.split('\t')
        label = label.strip()
        if label == 'lang1':
            label = 'en'
        if label == 'lang2':
            label = 'es'
        if tid not in offset_dict:
            offset_dict[tid] = ([], [], [])
        offset_dict[tid][0].append(int(off1))
        offset_dict[tid][1].append(int(off2))
        offset_dict[tid][2].append(label)
    tweet_dict = {}
    print('reading tweets')
    for line in tweet_lines:
        try:
            tid, user, tweet = line.split('\t')
            last_valid = tid
            tweet_dict[tid] = tweet
        except ValueError:
            tweet_dict[last_valid] += line

    labeled_dict = {}
    print('labeling tweets')
    for tid, tweet in tweet_dict.items():
        if tweet == "Not Found\n":
            continue
        offsets = offset_dict[tid]
        off1s = offsets[0]
        off2s = offsets[1]
        labels = offsets[2]
        text = [tweet[off1s[i]:off2s[i] + 1] for i in range(len(off1s))]
        assert len(text) == len(labels)
        text, labels = clean_data(text, labels)
        if not text or not labels:
            continue
        labeled_dict[tid] = {
            "text": text,
            "labels": labels
        }
    print("labeled", len(labeled_dict), "tweets")
    output_file = pair[2]
    print('writing files...')
    with open(output_file, 'w+') as output:
        json.dump(labeled_dict, output)
    parsed = None
    with open(output_file) as read_output:
        parsed = json.load(read_output)
    parsed = [(k, v) for k, v in parsed.items()]
    random.shuffle(parsed)
    #parsed = dict(parsed)
    with open(output_file, 'w+') as output:
        json.dump(parsed, output, indent=2, sort_keys=True)
    print()

