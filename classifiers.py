# here be some classifiers
import numpy as np
import pandas as pd
import torch
import torch.nn as nn
import torch.autograd as autograd
import torch.nn.functional as F

from difflib import get_close_matches
from torch import optim
from collections import Counter
from random import shuffle
from language_ex import *
from typing import List

from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.naive_bayes import MultinomialNB
from sklearn.pipeline import Pipeline

class LanguageClassifier(object):

    def predict(self, ex: List[str]) -> List[str]:
        raise Exception('Call my Implementations!')

    def fit(self, train: List[LabeledSentence]) -> None:
        raise Exception('Call my Implementations!')


class FrequencyClassifier(LanguageClassifier):

    def __init__(self):
        self.count_dict = {
            'en': Counter(),
            'es': Counter(),
            'other': Counter()
        }

    def predict(self, ex: List[str]) -> List[str]:
        predictions = []

        for w in ex:
            counts = [(self.count_dict[lbl][w], lbl) for lbl in self.count_dict]
            predictions.append(max(counts)[1])

        return predictions

    def fit(self, train: List[LabeledSentence]) -> None:

        for lbl_s in train:
            for word, label in lbl_s.get_lbled_w():
                self.count_dict[label][word] += 1

class NaiveBayesClassifier(LanguageClassifier):

    def __init__(self, lbl_idxer):
        self.text_clf = Pipeline([
            ('vect', CountVectorizer(ngram_range=(2, 10), analyzer='char', stop_words=None)),
            ('tdidf', TfidfTransformer(use_idf=True)),
            ('clf', MultinomialNB()),
        ])
        self.label_indexer = lbl_idxer

    def predict(self, ex: List[str]) -> List[str]:
        assert ex != []
        predictions = self.text_clf.predict(ex)
        return [self.label_indexer.get_object(pred) for pred in predictions]

    def fit(self, train: List[LabeledSentence]) -> None:
        obvs = []
        target = []

        for l_s in train:
            obvs.extend(l_s.words)
            target.extend(l_s.gold_label_vec(self.label_indexer))

        obvs = pd.Series(np.array(obvs))
        target = pd.Series(np.array(target))

        self.text_clf.fit(obvs, target)

def bachify(train_idxs, step=100):
    N = len(train_idxs)
    r = N % step
    for start in range(0, N - r, step):
        yield [train_idxs[i] for i in range(start, start + step)]

class RNNLM(nn.Module):
    def __init__(self, hid, out, num_emb, emb_size, padding_idx=0):
            super(RNNLM, self).__init__()
            self.hidden_size = hid
            self.label_size = out
            self.embed = nn.Embedding(num_emb, emb_size, padding_idx=padding_idx)
            self.lst = nn.LSTM(emb_size, hid, num_layers=2, batch_first=True, bidirectional=True, dropout=0.2)
            # don't count <PAD> tag as label
            self.W = nn.Linear(hid, self.label_size)
            self.log_softmax = nn.LogSoftmax(dim=2)
            # Initialize weights according to a formula due to Xavier Glorot.
            nn.init.xavier_uniform_(self.W.weight)

    def forward(self, x, x_lengths):
        # x_lengths contains the original length of every vector in batch x
        # in other words before batching
        batch_sz, seq_len = x.size()
        # NOTE: this is why you don't use max in _pad_ex_vecs
        embeds = torch.nn.utils.rnn.pack_padded_sequence(self.embed(x), x_lengths, batch_first=True)
        out, _ = self.lst(embeds)
        out, _ = torch.nn.utils.rnn.pad_packed_sequence(out, batch_first=True)
        # this is supposed to grab the states in the foward direction
        # NOTE: dim = (batch_size, seq_size, num_directions * hidden_size)
        forward = out[:, :, :self.hidden_size]
        backward = out[:, :, self.hidden_size:]
        # this is the code to that shift we talked about, but it makes performance pretty bad
        # need to take a look later and see whats up

        # for btch_num, (seq_ten, seq_len) in enumerate(zip(forward, x_lengths)):
        #     if seq_len - 1 >= 0:
        #         temp = torch.cat((torch.zeros(self.hidden_size).unsqueeze(0), seq_ten[:(seq_len-1)], seq_ten[seq_len:]))
        #         forward[btch_num] = temp

        # this is supposed to grab the second to last state in the backward direction
        # or the last one if index 1 is out of bounds
        # forward[:, 0] = out[:, 1 if forward.shape[1] > 1 else 0, self.hidden_size:]
        return self.W(torch.add(forward, backward))

class RNNClassifier(LanguageClassifier):

    def __init__(self, vocab_index, label_index, test_data=None, hddn_size=50,
                 num_epochs=15, learning_rate=0.001, optimizer=optim.Adam,
                 emb_vec_size=200, batch_size=100, save=None, load=None):
        self.vocab_idx = vocab_index
        self.vocab = [w for w in self.vocab_idx.objs_to_ints.keys()]
        self.label_idx = label_index
        self.padding_idx = self.label_idx.index_of('<PAD>')
        self.num_labels = len(self.label_idx)
        self.rnn = RNNLM(hddn_size, self.num_labels, len(vocab_index), emb_vec_size, self.padding_idx)
        if load:
            self.rnn.load_state_dict(torch.load(load))
            self.rnn.eval()
        self.num_epochs = num_epochs
        self.batch_size = batch_size
        self.optimizer = optimizer(self.rnn.parameters(), lr=learning_rate)
        self.test_data = test_data
        self.save = save

    def _print_test_accuracy(self):
        predictions = [self.predict(ls.words) for ls in self.test_data]
        golds = [ls.labels for ls in self.test_data]
        total = 0
        total_correct = 0
        for i in range(len(golds)):
            gold = golds[i]
            pred = predictions[i]
            for j in range(len(gold)):
                total += 1
                if gold[j] == pred[j]:
                    total_correct += 1
        print(f"Test Accuracy: {total_correct} / {total} = {round(total_correct/total, 6)}")

    def get_closest_word(self, w):
        top_match = []
        diff_per = 1.05
        while not top_match:
            diff_per -= 0.05
            top_match = get_close_matches(w, self.vocab, 1, diff_per)
        # print(f"closest match for '{w}' is '{top_match[0]}'")
        return self.vocab_idx.index_of(top_match[0])

    def predict(self, ex: List[str]) -> List[str]:
        self.rnn.eval()
        word_idxs = []
        for w in ex:
            idx = self.vocab_idx.index_of(w)
            if idx == -1:
                idx = self.get_closest_word(w)
            word_idxs.append(idx)
        idx_vec = np.array(word_idxs)
        raw_weights = self.rnn.forward(self.form_input(idx_vec).unsqueeze(0), [idx_vec.shape[0]])
        raw_weights = raw_weights.squeeze(0)

        probs = F.log_softmax(raw_weights, dim=1)
        prediction = [self.label_idx.get_object(label.item() + 1)
                        for label in torch.argmax(probs[:, 1:], dim=1)]
        return prediction

    def _pad_gold_vecs(self, gold_labels, max_len_in_btch=100):
        # may need it, don't really know
        gold_labels = sorted(gold_labels, key=lambda e: e.shape[0], reverse=True)
        # pad with 0s (<PAD>)
        gold_labels = np.array([np.pad(label_vec, (0, max_len_in_btch - label_vec.shape[0]), mode='constant')
                                for label_vec in gold_labels])
        return gold_labels

    def _pad_ex_vecs(self, exs):
        # do not change this to use the max function
        # just don't touch it
        exs = sorted(exs, key=lambda e: e.shape[0], reverse=True)
        original_ex_lens = [np_arry.shape[0] for np_arry in exs]
        max_len_in_btch = original_ex_lens[0]
        exs = np.array([np.pad(ex, (0, max_len_in_btch - ex.shape[0]), mode='constant')
                                for ex in exs])
        return exs, original_ex_lens

    def fit(self, train: List[LabeledSentence]) -> None:
        self.rnn.train()
        debug_logging = True
        test_acc = True

        train_exs = [np.array(l_s.word_idx_vec(self.vocab_idx)) for l_s in train]
        gold_label_exs = [np.array(l_s.gold_label_vec(self.label_idx)) for l_s in train]

        train_idxs = list(range(len(train_exs)))

        if debug_logging:
            print("Number of training observations: %i" % len(train_exs))

        for e in range(self.num_epochs):
            total_loss = 0
            shuffle(train_idxs)
            for idxs in bachify(train_idxs, step=self.batch_size):
                exs = [train_exs[train_idx] for train_idx in idxs]
                gold_labels = [gold_label_exs[train_idx] for train_idx in idxs]
                # pad all vectors with the enough zeros
                exs, og_ex_lens = self._pad_ex_vecs(exs)
                max_len_in_btch = og_ex_lens[0]
                gold_labels = self._pad_gold_vecs(gold_labels, max_len_in_btch)
                self.rnn.zero_grad()
                raw_weights = self.rnn.forward(self.form_input(exs), og_ex_lens)
                target = self.form_input(gold_labels)
                criterion = nn.CrossEntropyLoss(weight=torch.Tensor([1.0, 1.0, 1.25, 1.0]), ignore_index=self.padding_idx)
                # f_loss = criterion(forward.view(-1, self.num_labels), target.view(-1))
                # b_loss = criterion(backward.view(-1, self.num_labels), target.view(-1))
                loss = criterion(raw_weights.view(-1, self.num_labels), target.view(-1))
                total_loss += loss
                loss.backward()
                self.optimizer.step()
            if debug_logging and ( (e + 1) % 5 == 0 or e == 0):
                print("Total loss on epoch %i: %f" % ((e + 1), total_loss))
                if test_acc and self.test_data:
                    self.rnn.eval()
                    self._print_test_accuracy()
                    self.rnn.train()
                    yn = input("stop training? [y/N]: ")
                    if yn == "y":
                        break
        if self.save:
            print(f"saving trained model to: {self.save}")
            torch.save(self.rnn.state_dict(), self.save)
    def form_input(self, x):
        return torch.from_numpy(x).long()